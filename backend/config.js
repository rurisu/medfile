/**
 * Constants config project
 */

const PORT = process.env.PORT;
const DB_HOST = process.env.DB_HOST;
const DB_PORT = process.env.DB_PORT;
const DB_NAME = process.env.DB_NAME;
const TOKEN_SECRET = process.env.TOKEN_SECRET;

module.exports = {
    PORT,
    DB_HOST,
    DB_PORT,
    DB_NAME,
    TOKEN_SECRET
}