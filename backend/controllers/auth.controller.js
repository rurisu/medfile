/**
 * Authentication Controller
 */

var mongoose = require('mongoose'),
    User = mongoose.model('Usuario'),
    Credential = mongoose.model('Credenciales'),
    validations = require('../validations/auth.validation.js'),
    config = require('../config.js'),
    jwt = require('jsonwebtoken');

/**
* Login de usuario
*/
function login(req, res) {
    //Checkear el body
    let bodyErrors = validations.checkLogin(req);
    if (bodyErrors.length > 0) return res.status(400).send({
        errors: bodyErrors
    });

    //Find User by email
    Credential.findOne({
        nombreUsuario: new RegExp(`^${req.body.email}$`, 'i')
    }).select('+password').exec(function (err, cred) {
        if (err) return res.status(500).send({ errors: ["Ocurrió un error inesperado"]});

        if (!cred) return res.status(404).send({
            errors: ["No existe usuario"]
        });

        cred.comparePassword(req.body.password, async function (err, isMatch) {
            if (err) return res.status(500).send({ errors: ["Ocurrió un error inesperado"]});
    
            if (!isMatch) {
                return res.status(400).send({ errors: ["La contraseña no coincide"]});
            } else {
                //Obtener máxima hora para login con la credencial
                const maxTime = new Date(cred.fecha).getTime() + 1000 * 60 * 60;
                //Si la credencial es temporal, y la fecha actual es mayor a la máxima hora para la credencial => ERROR
                if(cred.isTemporal && new Date().getTime() > new Date(maxTime).getTime()) return res.status(400).send({ errors: ["Credencial ya expiró"]});

                const tokenData = {
                    id: cred._id
                }

                const token = jwt.sign(tokenData, config.TOKEN_SECRET, {
                    expiresIn: '5h'
                });

                return res.status(200).send({
                    "token": token,
                    "is_temporal": cred.isTemporal
                });
            }
        })
    
    });
}

/**
* Registrar un usuario
*/
function register(req, res) {
    //Checkear el body
    const bodyErrors = validations.checkRegister(req);
    if (bodyErrors.length > 0) return res.status(400).send({
        errors: bodyErrors
    });

    //Find User by email
    return Credential.findOne({
        nombreUsuario: new RegExp(`^${req.body.email}$`, 'i')
    }).exec(function (err, found) {
        if (err) return res.status(500).send({ errors: ["Ocurrió un error inesperado"]});

        if (found) return res.status(400).send({
            errors: ["Ya existe un usuario con ese email"]
        });

        const user = new User({
            nombre: req.body.nombre,
            apellido: req.body.apellido,
            email: req.body.email,
            fechaNac: req.body.fechaNac
        });

        user.save(function(err, savedUser){
            if (err) return res.status(500).send({ errors: ["Ocurrió un error al guardar la data"]})

            const credential = new Credential({
                nombreUsuario: req.body.email,
                password: req.body.password,
                isTemporal: false,
                usuarioId: savedUser._id,
                fecha: Date.now()
            });

            credential.save(function(err, _savedCredential){
                if (err) return res.status(500).send({ errors: ["Ocurrió un error al guardar la data"]})

                return res.status(200).send(savedUser);
            });
        });
    });
}

module.exports = {
    register,
    login
}