var mongoose = require('mongoose'),
    validations = require('../validations/user.validation.js'),
    Usuario = mongoose.model('Usuario'),
    Credential = mongoose.model('Credenciales'),
    Exam = mongoose.model('Examen'),
    Receipt = mongoose.model('Prescripcion'),
    Speciality = mongoose.model('Especialidad'),
    moment = require('moment'),
    fs = require('fs'),
    randomatic = require('randomatic');

/**
* Obtener usuario logueado
*/
function getMe(req, res) {
    return res.status(200).send({ response: req.user });
}

function editProfile(req, res){
    const bodyErrors = validations.checkEditProfile(req);
    if (bodyErrors.length > 0) return res.status(400).send({
        errors: bodyErrors
    });

    return Usuario.findById(req.user.usuarioId).exec(function(err, user){
        if (err) return res.status(500).send({ errors: ["Ocurrió un error inesperado"]});

        if (!user) return res.status(404).send({
            errors: ["No existe usuario"]
        });

        user.nombre = req.body.nombre;
        user.apellido = req.body.apellido;
        user.fechaNac = req.body.fechaNac;
        user.genero = req.body.genero;
        user.prevision = req.body.prevision;
        user.tipoSangre = req.body.tipoSangre;

        user.save(function(err, saved){
            if (err) return res.status(500).send({ errors: ["Ocurrió un error inesperado"]});

            return res.status(200).send({response: saved})
        });
    });
}

function editRecord(req, res){
    const bodyErrors = validations.checkEditRecord(req);
    if (bodyErrors.length > 0) return res.status(400).send({
        errors: bodyErrors
    });

    return Usuario.findById(req.user.usuarioId).exec(function(err, user){
        if (err) return res.status(500).send({ errors: ["Ocurrió un error inesperado"]});

        if (!user) return res.status(404).send({
            errors: ["No existe usuario"]
        });

        user.antecedentes.alergias = req.body.alergias;
        user.antecedentes.cirugias = req.body.cirugias;
        user.antecedentes.medicamentos = req.body.medicamentos;
        user.antecedentes.diabetes = req.body.diabetes;
        user.antecedentes.tiroides = req.body.tiroides;
        user.antecedentes.hipertension = req.body.hipertension;
        user.antecedentes.cardiopatias = req.body.cardiopatias;

        return user.save(function(err, saved){
            if (err) return res.status(500).send({ errors: ["Ocurrió un error inesperado"]});

            return res.status(200).send({response: user})
        });
    });
}

function uploadExam(req, res){
    if (!req.file) return res.status(500).send({ errors: ["Ocurrió un error al subir el archivo"]});

    const bodyErrors = validations.checkUploadExam(req);
    if (bodyErrors.length > 0) {
        fs.unlink(req.file.path, function(){});
        return res.status(400).send({
            errors: bodyErrors
        });
    }
    
    Speciality.findById(req.body.especialidad_id).exec(function(err, specialityFound){
        if (err) {
            fs.unlink(req.file.path, function(){});
            return res.status(500).send({ errors: ["Ocurrió un error inesperado"]});
        }

        if(!specialityFound) {
            fs.unlink(req.file.path, function(){});
            return res.status(404).send({ errors: ["Especialidad no encontrada"]});
        }

        var exam = new Exam({
            fecha: req.body.fecha,
            especialidadId: specialityFound._id,
            usuarioId: req.user.usuarioId._id,
            file: req.file.filename
        });

        exam.save(function(err, saved){
            if (err) {
                fs.unlink(req.file.path, function(){});
                return res.status(500).send({ errors: ["Ocurrió un error inesperado"]});
            }

            return res.status(200).send({saved});
        })
    });
}

function getExams(req, res){
    var query = { usuarioId: req.user.usuarioId._id }
    if(req.user.isTemporal){
        query["visibilidad"] = true;
    }

    Exam.find(query).populate('especialidadId').exec(function(err, exams){
        if (err) return res.status(500).send({ errors: ["Ocurrió un error inesperado"]});

        return res.status(200).send(exams);
    });
}

function deleteExam(req, res){
    Exam.findByIdAndDelete(req.params.idExam).exec(function(err, exam){
        if (err) return res.status(500).send({ errors: ["Ocurrió un error inesperado"]});
        if (!exam) return res.status(404).send({ errors: ["Examen no encontrado"]});

        fs.unlink(`files/${exam.file}`, function(){});

        return res.status(200).send({success: true});
    });
}

function uploadReceipt(req, res){
    if (!req.file) return res.status(500).send({ errors: ["Ocurrió un error al subir el archivo"]});

    var receipt = new Receipt({
        observacion: req.body.observacion,
        usuarioId: req.user.usuarioId._id,
        file: req.file.filename
    });

    receipt.save(function(err, saved){
        if (err) {
            fs.unlink(req.file.path, function(){});
            return res.status(500).send({ errors: ["Ocurrió un error inesperado"]});
        }

        return res.status(200).send({saved});
    })
}

function getReceipts(req, res){
    var query = { usuarioId: req.user.usuarioId._id }
    if(req.user.isTemporal){
        query["visibilidad"] = true;
    }

    Receipt.find(query).exec(function(err, receipts){
        if (err) return res.status(500).send({ errors: ["Ocurrió un error inesperado"]});


        return res.status(200).send(receipts);
    });
}

function deleteReceipt(req, res){
    Receipt.findByIdAndDelete(req.params.idReceipt).exec(function(err, receipt){
        if (err) return res.status(500).send({ errors: ["Ocurrió un error inesperado"]});
        if (!receipt) return res.status(404).send({ errors: ["Prescripción no encontrada"]});

        fs.unlink(`files/${receipt.file}`, function(){});

        return res.status(200).send({success: true});
    });
}

function generateCredentials(req, res){
    const generate_user = randomatic('a0', 8);
    const generate_password = randomatic('Aa0', 8);

    const credential = new Credential({
        nombreUsuario: generate_user,
        password: generate_password,
        fecha: Date.now(),
        isTemporal: true,
        usuarioId: req.user.usuarioId._id
    });

    credential.save(function(err, savedCredential){
        if (err) return res.status(500).send({ errors: ["Ocurrió un error inesperado"]});

        return res.status(200).send({ response: {
            nombreUsuario: generate_user,
            password: generate_password,
            fecha: moment(credential.fecha).add(1, 'hour')
        }})
    })
}

function visibilityExam(req, res){
    Exam.findById(req.params.idExam).exec(function(err, exam){
        if (err) return res.status(500).send({ errors: ["Ocurrió un error inesperado"]});
        if (!exam) return res.status(404).send({ errors: ["Examen no encontrado"]});

        exam.visibilidad = !exam.visibilidad;

        exam.save(function(err, saved){
            if (err) return res.status(500).send({ errors: ["Ocurrió un error inesperado"]});

            return res.status(200).send({visibility: exam.visibilidad});
        });
    });
}

function visibilityPresc(req, res){
    Receipt.findById(req.params.idReceipt).exec(function(err, receipt){
        if (err) return res.status(500).send({ errors: ["Ocurrió un error inesperado"]});
        if (!receipt) return res.status(404).send({ errors: ["Prescripción no encontrada"]});

        receipt.visibilidad = !receipt.visibilidad;

        receipt.save(function(err, saved){
            if (err) return res.status(500).send({ errors: ["Ocurrió un error inesperado"]});

            return res.status(200).send({visibility: saved.visibilidad});
        });
    });
}

module.exports = {
    getMe,
    editProfile,
    editRecord,
    generateCredentials,
    uploadExam,
    getExams,
    deleteExam,
    uploadReceipt,
    getReceipts,
    deleteReceipt,
    visibilityExam,
    visibilityPresc
}