const jwt = require('jsonwebtoken'),
    config = require('../config.js'),
    mongoose = require('mongoose'),
    Credential = mongoose.model('Credenciales');

/**
 * Saber si el token es válido
 */
function isAuth (req, res, next) {
    const authorization = req.headers['authorization'];

    if (!authorization) return res.status(401).send({errors: ["Falta token"]})

    const token = authorization.split('Bearer ');
    if (!token[1]) return res.status(401).send({errors: ["Falta token"]})

    jwt.verify(token[1], config.TOKEN_SECRET, function (err, decoded) {
        if (err) return res.status(401).send({errors: ["Error al decodificar token"]})

        Credential.findById(decoded.id).populate('usuarioId').exec(function (err, user) {
            if (err) return res.status(401).send({errors: ["Error al decodificar token"]})
            if (!user) return res.status(401).send({errors: ["Error al decodificar token"]})

            req.user = user;
            return next();
        });
    });
}

function isNotTemporal (req, res, next) {
    if(!req.user.isTemporal){
        next();
    } else {
        return res.status(403).send({errors: ["No autorizado con estas credenciales"]})
    }
}

module.exports = {
    isAuth,
    isNotTemporal
}
