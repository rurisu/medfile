/**
 * Auth Routes
 */

const express = require("express"),
    authRouter = express.Router(),
    //Controllers
    authController = require("../controllers/auth.controller.js");

authRouter
    .post("/register", authController.register)
    .post("/login", authController.login);

module.exports = authRouter;