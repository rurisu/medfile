const multer = require('multer');
const path = require('path');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, "files/")
    },
    filename: (req, file, cb) => {
        const datetimestamp = Date.now();
        const fileName = file.originalname.replace(" ", "_");
        cb(null,  fileName.split('.')[0] + '-' + datetimestamp + '.' + fileName.split('.')[fileName.split('.').length - 1])
    },
})

const upload = multer({
    storage: storage,
    fileFilter: (req, file, cb) => {
        if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg") {
            cb(null, true);
        } else if (
            file.mimetype == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" ||
            file.mimetype == "application/vnd.ms-excel" ||
            file.mimetype == "application/pdf" || 
            file.mimetype == "application/msword") {
            cb(null, true);
        } else {
            return cb({ message: 'Solo se pueden subir .png, .jpg .jpeg .xls .xlsx .pdf .doc'});
        }
    },
    limits: { fileSize: '5MB' }
}).single('file')

function getFile(req, res){
    return res.sendFile(path.join(__dirname, `../files/${req.params.file}`))
}

module.exports = {
    upload,
    getFile
}