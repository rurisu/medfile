var validator = require('validator');
var moment = require('moment');

module.exports = {
    //LOGIN
    checkEditProfile: function (req) {
        let error = [];
        if (!req.body.nombre || validator.isEmpty(req.body.nombre + "")) error.push("Falta nombre");
        if (!req.body.apellido || validator.isEmpty(req.body.apellido + "")) error.push("Falta apellido");
        if (!req.body.genero || validator.isEmpty(req.body.genero + "")) error.push("Falta genero");
        if (!req.body.fechaNac || validator.isEmpty(req.body.fechaNac + "")) error.push("Falta fecha de nacimiento");
        else {
            var date = new Date(req.body.fechaNac);
            if (!moment(date, 'YYYY-MM-DD', true).isValid()) error.push("La fecha de nacimiento debe ser válida (YYYY-MM-DD)");
        }

        if(req.body.prevision && (req.body.prevision !== 'F' && req.body.prevision !== 'I')){
            error.push("La previsión debe ser 'F', 'I' (Fonasa, Isapre)")
        }

        if(req.body.genero && (req.body.genero !== 'F' && req.body.genero !== 'M' && req.body.genero !== 'O')){
            error.push("El genero debe ser 'F', 'M', 'O' (Femenino, Masculino, Otro)")
        }

        if(req.body.tipoSangre && 
            (req.body.tipoSangre !== 'O+' && 
            req.body.tipoSangre !== 'O-' && 
            req.body.tipoSangre !== 'A-' && 
            req.body.tipoSangre !== 'A+' && 
            req.body.tipoSangre !== 'B-' &&
            req.body.tipoSangre !== 'B+' &&
            req.body.tipoSangre !== 'AB-' && 
            req.body.tipoSangre !== 'AB+')
        ){
            error.push("Tipo de sangre debe ser alguno de estos O+  O-  A-  A+  B-  B+  AB- AB+")
        }

        return error;
    },

    //REGISTER
    checkEditRecord: function (req) {
        let error = [];

        if (validator.isEmpty(req.body.diabetes + "")) error.push("Falta diabetes");
        else if (!validator.isBoolean(req.body.diabetes + "")) error.push("diabetes debe ser un booleano");

        if (validator.isEmpty(req.body.tiroides + "")) error.push("Falta tiroides");
        else if (!validator.isBoolean(req.body.tiroides + "")) error.push("tiroides debe ser un booleano");

        if (validator.isEmpty(req.body.hipertension + "")) error.push("Falta hipertension");
        else if (!validator.isBoolean(req.body.hipertension + "")) error.push("hipertension debe ser un booleano");

        if (validator.isEmpty(req.body.cardiopatias + "")) error.push("Falta cardiopatias");
        else if (!validator.isBoolean(req.body.cardiopatias + "")) error.push("cardiopatias debe ser un booleano");

        return error;
    },

    checkUploadExam: function (req) {
        let error = [];

        if (!req.body.fecha || validator.isEmpty(req.body.fecha + "")) error.push("Falta fecha");
        if (!req.body.especialidad_id || validator.isEmpty(req.body.especialidad_id + "")) error.push("Falta especialidad_id");
        return error;
    }
}