import Vue from 'vue';
import VueRouter from 'vue-router';
import routes from './routes';

Vue.use(VueRouter);

// configure router
const router = new VueRouter({
  routes, // short for routes: routes
  linkActiveClass: 'active',
  scrollBehavior: (to, from ,savedPosition) => {
    if (savedPosition) {
      return savedPosition;
    }
    if (to.hash) {
      return { selector: to.hash };
    }
    return { x: 0, y: 0 };
  }
});

router.beforeEach((to, from, next) => {
  const isAuthenticated = localStorage.getItem('token', null);
  Vue.prototype.$isTemporal = localStorage.getItem('is_temporal', '0') === '0' ? false : true;
  if (to.name !== 'login' && to.name !== 'register' && !isAuthenticated) next({ name: 'login' });
  else next();
})

export default router;
