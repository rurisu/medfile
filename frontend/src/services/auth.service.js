import { handleResponse } from './helper';

const apiUrl = 'http://localhost:5000';

function login(correo, contraseña) {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "email": correo,
            "password": contraseña
        })
    };

    return fetch(apiUrl + '/auth/login', requestOptions)
        .then(handleResponse)
        .then(res => { return res; });
}

function register(user) {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(user)
    };

    return fetch(apiUrl + '/auth/register', requestOptions)
        .then(handleResponse)
        .then(res => { return res; });
}

export const authService = {
    login,
    register,
};